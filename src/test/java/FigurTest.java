import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FigurTest {

        @Test
        void KriegerMitAxt() {
                Figur figur = new Krieger("Ben", new AxtVerhalten());
                assertEquals(figur.kämpfen(),"Ben der Krieger schlägt mit einer Axt zu");
        }

        @Test
        void SchurkeMitDolch() {
                Figur figur = new Schurke("Jim", new DolchVerhalten());
                assertEquals(figur.kämpfen(),"Jim der Schurke schlägt mit einem Dolch zu");
        }
}