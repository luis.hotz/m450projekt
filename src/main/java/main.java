public class main {
    public static void main(String[] args) {
        Figur Char1 = new Krieger("John", new AxtVerhalten());
        Figur Char2 = new Magier("Heidi", new StabVerhalten());
        Figur Char3 = new Schurke("Tom", new DolchVerhalten());
        Char1.kämpfen();
        Char2.kämpfen();
        Char3.kämpfen();
    }
}
