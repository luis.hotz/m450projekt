public class Figur {
    private String characterName;
    private IWaffenVerhalten waffe;

    public Figur(String characterName, IWaffenVerhalten waffe) {
        this.characterName = characterName;
        this.waffe = waffe;
    }
    public String kämpfen() {
        String className = this.getClass().getName();
        String displayText = characterName + " der " + className + " schlägt " + waffe.verwendeWaffe() + " zu";
        return displayText;
    }
}
