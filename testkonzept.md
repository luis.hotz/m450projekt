INTRODUCTION:

Kleines Programm welches einen mit input einen Satz bildet.

TESTS ITEMS: 

Zwei Sätze sind vom Programm zu bilden.

FEATURES TO BE TESTED: 

Das einzige existierende Feature, welches einen kleinen Satz bildet, soll getested werde.

APPROACH: 

Ich mach Unit tests. 

ITEM PASS/FAIL CRITERIA:

Fail: Satz ist Falsch, Kann nicht ausgeführt werden
PASS: Satz ist Richtig

TEST DELIVERABLES:

Tests mit JUnit, gitlab ci pipeline sollte auch erstellt werden.

TESTING TASKS:

gibt nur Unit tests.

ENVIRONMENTAL NEEDS:

GitLab für pipeline.

SCHEDULE:

Tests sollten vor jedem commit ablaufen.

    